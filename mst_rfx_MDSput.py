#! /usr/bin/env python2.7

#  (C) Copyright 2016 Cristiano Lino Fontana

from __future__ import print_function, with_statement

import datetime
import argparse
import zmq
import struct
import MDSplus as mds

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]

event_PSD_struct = struct.Struct("<QHHHBB")
print("Struct size: {:d}".format(event_PSD_struct.size))

parser = argparse.ArgumentParser(description='Read and print ABCD events from status sockets')
parser.add_argument('-A',
                    '--data_socket',
                    type = str,
                    help = 'Data socket address',
                    default = "tcp://127.0.0.1:16181")
parser.add_argument('-T',
                    '--polling_time',
                    type = float,
                    help = 'Socket polling time',
                    default = 200)
parser.add_argument('-o',
                    '--output_file',
                    type = str,
                    default = "log/events_{}.txt".format(datetime.datetime.now().strftime("%Y%m%d")),
                    help = 'Output file')

args = parser.parse_args()

topic = "data_abcd".encode('ascii')

print("Saving to: {}".format(args.output_file))
print("Connecting to: {}".format(args.data_socket))
print("Topic: {}".format(topic))

with zmq.Context() as context:
    poller = zmq.Poller()

    socket = context.socket(zmq.SUB)

    socket.connect(args.data_socket)
    socket.setsockopt(zmq.SUBSCRIBE, topic)

    poller.register(socket, zmq.POLLIN)

    i = 0
    try:
        while True:
            socks = dict(poller.poll(args.polling_time))

            if socket in socks and socks[socket] == zmq.POLLIN:
                print("Message received!")

                binary_message = socket.recv()
                separator_index = binary_message.decode('ascii', errors = 'ignore').find(' ')

                if separator_index == -1:
                    print("ERROR: Unable to find separator")
                else:
                    topic = binary_message[0:separator_index].decode('ascii', errors = 'ignore')

                    print("Topic: {}".format(topic))

                    binary_data = binary_message[separator_index + 1:]
                    binary_size = len(binary_data)

                    print("Data length: {:d}, events: {:d}, remainder: {:d}".format(binary_size, binary_size // 16, binary_size % 16))

                    print("Reading the message data")
                    timestamps = list()
                    channels = list()
                    qshorts = list()
                    qlongs = list()

                    try:
                        for event_buffer in chunks(binary_data, 16):
                            unpacked = event_PSD_struct.unpack(event_buffer)

                            timestamp, qshort, qlong, baseline, channel, pur = unpacked

                            timestamps.append(timestamp)
                            channels.append(channel)
                            qshorts.append(qshort)
                            qlongs.append(qlong)
                    except Exception as error:
                        print("ERROR: {}: {}".format(error.__class__.__name__, str(error)))

                    delta_time = int(timestamps[-1]) - int(timestamps[0])

                    print("Packet time width: {:d}, {:f}".format(delta_time, delta_time * 1e-9 * 4 / 1024))

                    try:
                        current_shot = 0

                        with mds.Connection('aurora.physics.wisc.edu') as connection:
                            connection.openTree('MST', 0, mode = 'ReadOnly')
                            current_shot = int(connection.get('$SHOT'))

                        # This part is not working, for some reason
                        #rfx_tree = mds.Tree('rfx_gamma', current_shot, mode = 'Normal')
                        #print("Getting the nodes")
                        ##timestamps_node = rfx_tree.getNode('\\TIMESTAMP')
                        #channels_node = rfx_tree.getNode('\\CHANNEL')
                        #qshorts_node = rfx_tree.getNode('\\QSHORT')
                        #qlongs_node = rfx_tree.getNode('\\QLONG')

                        #print("Putting the data into the tree")
                        #with mds.Connection('eos.physics.wisc.edu') as connection:
                        #    print("Opening the tree with current_shot: {:d}".format(current_shot))
                        #    connection.openTree('rfx_gamma', current_shot, mode = 'Normal')

                        #    print("Putting timestamp signal")
                        #    connection.put('\\TIMESTAMP', 'BUILD_SIGNAL($1,$2)', mds.Uint64Array(timestamps), mds.Uint64Array(timestamps))

                        #    print("Putting channel signal")
                        #    connection.put('\\CHANNEL', 'BUILD_SIGNAL($1,$2)', mds.Uint8Array(channels), mds.Uint64Array(timestamps))

                        #    print("Putting qshort signal")
                        #    connection.put('\\QSHORT', 'BUILD_SIGNAL($1,$2)', mds.Uint16Array(qshorts), mds.Uint64Array(timestamps))

                        #    print("Putting qlong signal")
                        #    connection.put('\\QLONG', 'BUILD_SIGNAL($1,$2)', mds.Uint16Array(qlongs), mds.Uint64Array(timestamps))

                        #    print("Done with the putting!")

                        # On the other hand this part is working!!!
                        with mds.Connection('aurora.physics.wisc.edu') as connection:
                            print("Opening the tree with current_shot: {:d}".format(current_shot))
                            connection.openTree('mst_rf', current_shot, mode = 'Normal')

                            print("Putting timestamp signal")
                            connection.put('.proc_rf:gam_timestmp', 'BUILD_SIGNAL($1,$2)', mds.Uint64Array(timestamps), mds.Uint64Array(timestamps))

                            print("Putting channel signal")
                            connection.put('.proc_rf:gam_channel', 'BUILD_SIGNAL($1,$2)', mds.Uint8Array(channels), mds.Uint64Array(timestamps))

                            print("Putting qshort signal")
                            connection.put('.proc_rf:gam_qshort', 'BUILD_SIGNAL($1,$2)', mds.Uint16Array(qshorts), mds.Uint64Array(timestamps))

                            print("Putting qlong signal")
                            connection.put('.proc_rf:gam_qlong', 'BUILD_SIGNAL($1,$2)', mds.Uint16Array(qlongs), mds.Uint64Array(timestamps))

                            print("Done with the putting!")

                    except Exception as error:
                        print("ERROR: {}: {}".format(error.__class__.__name__, str(error)))

    except KeyboardInterrupt:
        socket.close()
    except Exception as error:
        print("ERROR: {}: {}".format(error.__class__.__name__, str(error)))
        socket.close()
