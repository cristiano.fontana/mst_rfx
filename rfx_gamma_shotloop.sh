#!/bin/bash
## a cut down version of the mcs shotloop, here arms the rfx gamma detector.


#make sure DIO32 can read the input from PLC
acqcmd -s 2 set.dio32 0000000000000000----------------
echo "-- reset DIO channels --"
# now loop forever
#this will exit on a CTRL-C
while :
do
	# check for for the -5sec signal
# THIS IS RUNNING
	shotcoming=$(acqcmd -s 2 get.dio32 |cut -c17)
#	echo "checking for shot coming : ",$shotcoming
	# THIS IS FOR TESTING WITH LOOPBACK shotcoming=$(acqcmd -s 2 get.dio32 |cut -c18)
	# echo "shotcoming :"  $shotcoming

	if  [ "H" = $shotcoming ]
	then
		echo "shot in 5 sec... "
		sleep 1
		echo '--> Sending start to gamma-detection <--'
		python send_command.py -C 'tcp://128.104.166.113:16182' start
		#wait
#		echo '*** Zeroing  AO, DIO outputs after shot ***'
		acqcmd -s 2 set.dio32 0000000000000000----------------
		sleep 10
		python send_command.py -C 'tcp://128.104.166.113:16182' stop
#    the 20 sec wait for plc to reset the enable        
		sleep 20
	fi
	sleep 1
	echo -n '.'

done
