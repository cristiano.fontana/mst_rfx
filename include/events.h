#ifndef __EVENTS_H__
#define __EVENTS_H__ 1

#include <stdint.h>

struct event_PSD
{
    // It is necessary to have the size of this struct a multiple of 64 bits.
    // Thus we can be more confident that we have a binary structure
    // corresponding to the expectations.

    const uint64_t timestamp;
    const uint16_t qshort;
    const uint16_t qlong;
    const uint16_t baseline;
    const uint8_t channel;
    const uint8_t pur;
};

#endif
