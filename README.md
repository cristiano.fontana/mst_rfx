# MST RFX DAQ guide
## DAQ Guide
### Computers parameters
RFX computer:
* Host name: `neutron-rfx`
* IP: `128.104.166.112`
* User name: `acq-rfx`

MST borrowed computer:
* Host name: `neutron2-rfx`
* IP: `128.104.166.113`
* User name: `acq-rfx`

To connect to the computer through the terminal:

    ssh -Y acq-rfx@128.104.166.112

### DAQ Connection
The DAQ is hosted on `neutron2-rfx` at the time being.
To access the DAQ interface connect to: [http://128.104.166.113:8080](http://128.104.166.113:8080)

### DAQ startup
The OS in the computer should start the DAQ automatically during the boot sequence.
If the DAQ is not started, it is possible to start it with the command

    /home/acq-rfx/abcd/this_startup.sh

The output should be something like:

    Today is 20180314
    Starting a new ABCD session
    Creating EFG window
    ABCD:1.0
    Creating loggers window
    ABCD:2.0
    ABCD:2.1
    ABCD:2.1
    ABCD:2.1
    Waiting for node.js to start
    Creating ABCD window
    ABCD:3.0
    Creating HIJK window
    Creating LMNO window, folder: /home/acq-rfx/abcd_data/
    ABCD:4.0
    Creating PQRS window
    ABCD:5.0
    Creating WaFi window
    ABCD:6.0
    Creating EnFi window
    ABCD:7.0

### DAQ Stop
To completely stop the DAQ run the command

    /home/acq-rfx/abcd/stop_ABCD.sh

### Saving files
Files can be manually saved with the ABCD format, in case a user does not want to push data in the MDSplus trees.
Data files must be **opened before the start** of an acquisition, otherwise the data is lost.
The LMNO tab is the user interface for the data saving.
The user must specify the file name, files are saved in the folder:

    /home/acq-rfx/abcd_data/
    
File types:
* **Events files** are space-efficient files with only the PSD parameters acquired by the digitizer. Refer to the ABCD documentation for the [file format](https://gitlab.com/cristiano.fontana/abcd#psd-events-binary-protocols).
* **Waveforms files** save only the waveforms recorded by the digitizer, they tend to be very memory consuming. Refer to the ABCD documentation for the [file format](https://gitlab.com/cristiano.fontana/abcd#waveforms-binary-protocols).
* **Raw files** save both the events and waveforms in a rather complex format. They are used with the [replay_raw](https://gitlab.com/cristiano.fontana/abcd/tree/master/replay_raw) program to simulate off-line a full working system.

### Specific MST modules
There are two new modules specific to the MST setup:
* `mst_rfx_filter`: reads the datastream and looks for a trigger signal.
  When the trigger signal is seen it starts to accumulate data until the timestamps exit the coincidence window (approx 200 ms).
  The accumulated data is then forwarded as a single packet to the `mst_rfx_MDSput` module.
* `mst_rfx_MDSput`: reads the datastream from `mst_rfx_filter`, when a packet is received it pushes it to the MDSplus trees.

They should both run continuously and do not need to be restarted at each shot.

## HV parameters
Once connected to the computer run

    CAENGECO2020

The connection parameters are:
* Power supply type: DT55XXE HV
* Connection type: VCP USB
* Port: `ttyACM0` (usually)
* Baud: 9600
* Data: 8
* Stop: 1
* Parity: None

Usually the first time it is started it does not work, therefore try a couple of times.
The port might change, to check that run the command

    ls /dev/tty*

HV ports are usually of the format `ttyACM*`.

If the HV module is connected throught the ethernet port the ip is

    128.104.166.111

## Plotting results
There are some python scripts that can be used to display the events files.
* `~/abcd/bin/plot_Evst.py` displays the evolution in time of the energy spectrum. It also plots the evolution of the data rate in time. This is very useful to diagnose if the DAQ has some dead time.
* `~/abcd/bin/plot_PSD.py` displays the PSD bidimensional histogram.

All the scripts have an in-line help, just call them with the `-h` flag, _e.g._:

    acq-rfx@neutron-rfx:~$ ~/abcd/bin/plot_Evst.py -h
    usage: plot_Evst.py [-h] [-l] [-n NS_PER_SAMPLE] [-N EVENTS_COUNT]
                        [-r TIME_RESOLUTION] [-t TIME_MIN] [-T TIME_MAX]
                        [-R ENERGY_RESOLUTION] [-e ENERGY_MIN] [-E ENERGY_MAX]
                        [-s SAVE_DATA]
                        file_name channel

    Read and print ABCD status

    positional arguments:
      file_name             Input file name
      channel               Channel selection (all or number)

    optional arguments:
      -h, --help            show this help message and exit
      -l, --logscale        Display spectra in logscale
      -n NS_PER_SAMPLE, --ns_per_sample NS_PER_SAMPLE
                            Nanoseconds per sample (default: 0.001953)
      -N EVENTS_COUNT, --events_count EVENTS_COUNT
                            Events to be read from file (default: -1.000000)
      -r TIME_RESOLUTION, --time_resolution TIME_RESOLUTION
                            Time resolution (default: 0.200000)
      -t TIME_MIN, --time_min TIME_MIN
                            Time min (default: -1.000000)
      -T TIME_MAX, --time_max TIME_MAX
                            Time max (default: -1.000000)
      -R ENERGY_RESOLUTION, --energy_resolution ENERGY_RESOLUTION
                            Energy resolution (default: 20.000000)
      -e ENERGY_MIN, --energy_min ENERGY_MIN
                            Energy min (default: 400.000000)
      -E ENERGY_MAX, --energy_max ENERGY_MAX
                            Energy max (default: 20000.000000)
      -s SAVE_DATA, --save_data SAVE_DATA
                            Save histograms to file

## File conversion
The `~/abcd/bin/read_data.py` script converts the events files to an ASCII file with the format:

    #N      timestamp       qshort  qlong   channel
    0       3403941888      1532    1760    4
    1       3615693824      471     561     4
    2       4078839808      210     268     4
    3       4961184768      198     216     4
    4       6212482048      775     892     4
    ...     ...             ...     ...     ...

## Turn on procedure
1. Turn on the digitizer.
2. Turn on the HV module.
3. Turn on both PCs.
4. Verify that on `neutron2-rfx` the DAQ has started
   1. Connect to `neutron2-rfx`: `ssh -Y acq-rfx@128.104.166.113`
   2. Verify that there is an ABCD tmux session: `tmux ls`
      The session has to have 10 windows.
   3. If you really want to be sure...
   4. Attach to the ABCD session: `tmux a -t ABCD`
   5. If on the bottom line there is an `abcd` window then the DAQ is properly started
   6. Detach from the session with: `ctrl-b + d`
   7. If the DAQ has not started it might be due to a miscommunication with the digitizer, or the digitizer is turned off.
   8. To start manually the DAQ run: `/home/acq-rfx/abcd/this_startup.sh`
5. Turn on the HVs
   1. Connect to `neutron-rfx`: `ssh -Y acq-rfx@128.104.166.112`
   2. Run `CAENGECO2020` to turn on the HVs.
6. Done!

## Shutdown procedure
1. **Turn off the HVs!!!**
   1. Connect to `neutron-rfx`: `ssh -Y acq-rfx@128.104.166.112`
   2. Run `CAENGECO2020` to turn off the HVs.
2. Shut down all the PCs:
   1. Connect to `neutron-rfx`: `ssh -Y acq-rfx@128.104.166.112`
   2. Get root priviledges: `su`
   3. Turn off the computer: `shutdown -h now`
   4. Connect to `neutron2-rfx`: `ssh -Y acq-rfx@128.104.166.113`
   5. Get root priviledges: `su`
   6. Turn off the computer: `shutdown -h now`
3. Done!
