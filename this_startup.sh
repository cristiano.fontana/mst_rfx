#! /bin/bash
# 
# Example of startup script

# Folder in which ABCD is installed
FOLDER="$HOME""/abcd/"
# Folder in which data should be saved
DATA_FOLDER="$HOME""/abcd_data/"

TODAY="`date "+%Y%m%d"`"
echo 'Today is '"$TODAY"

# Unsetting $TMUX in order to be able to launch new sessions from tmux
unset TMUX

#This is needed only on older versions of tmux, if the -c option does not work
#echo "Changing folder to: ""$FOLDER"
#cd "$FOLDER"

# Checking if another ABCD session is running
if [ "`tmux ls 2> /dev/null | grep ABCD | wc -l`" -gt 0 ]
then
    echo "Kiling previous ABCD sessions"
    tmux kill-session -t ABCD
    sleep 2
fi

echo "Starting a new ABCD session"
tmux new-session -d -s ABCD

echo "Creating EFG window"
tmux new-window -d -c "$FOLDER""/efg/" -P -t ABCD -n efg 'node efg.js'

echo "Creating loggers window"
tmux new-window -d -c "$FOLDER" -P -t ABCD -n loggers "./bin/read_events.py -S 'tcp://127.0.0.1:16180' -o log/abcd_events_""$TODAY"".log"
tmux split-window -d -c "$FOLDER" -P -t ABCD:2.0 -h "./bin/read_events.py -S 'tcp://127.0.0.1:16183' -o log/hijk_events_""$TODAY"".log"
tmux split-window -d -c "$FOLDER" -P -t ABCD:2.0 -h "./bin/read_events.py -S 'tcp://127.0.0.1:16185' -o log/lmno_events_""$TODAY"".log"
tmux split-window -d -c "$FOLDER" -P -t ABCD:2.0 -h "./bin/read_events.py -S 'tcp://127.0.0.1:16187' -o log/pqrs_events_""$TODAY"".log"

tmux select-layout -t ABCD:2 even-vertical

echo "Waiting for node.js to start"
sleep 2

echo "Creating ABCD window"
# Example for a desktop digitizer
tmux new-window -d -c "$FOLDER" -P -t ABCD -n abcd './abcd/abcd -c 1 -l 0 -n 0 -B 4096 -T 5 -f abcd/DT5725_DPP-PSD_MST.json'

echo "Creating HIJK window"
# Connection options are the same as abcd
#tmux new-window -d -c "$FOLDER" -P -t ABCD -n hijk './hivo/hivo -c 0 -l 0 -V "0x00590000" -T 500 -f hivo/this_config.json'

echo "Creating LMNO window, folder: ""$DATA_FOLDER"
tmux new-window -d -c "$DATA_FOLDER" -P -t ABCD -n lmno "$FOLDER"'/lmno/lmno'

echo "Creating PQRS window"
tmux new-window -d -c "$FOLDER" -P -t ABCD -n pqrs './pqrs/pqrs'

echo "Creating WaFi window"
tmux new-window -d -c "$FOLDER" -P -t ABCD -n wafi './wafi/wafi -v -T 100'

echo "Creating EnFi window"
tmux new-window -d -c "$FOLDER" -P -t ABCD -n enfi './enfi/enfi -v -T 100'

echo "Creating MST-RFX filter window"
tmux new-window -d -c "$FOLDER" -P -t ABCD -n mst_rfx_filter './mst_rfx/mst_rfx_filter -v -n .0039062500 -r 200 5'

echo "Creating MST-RFX MDSput window"
tmux new-window -d -c "$FOLDER" -P -t ABCD -n mst_rfx_MDSput './mst_rfx/mst_rfx_MDSput.py -A "tcp://127.0.0.1:17181"'

# In case it is needed to start a browser as well
#echo "Opening browser on EFG page"
#xdg-open 'http://localhost:8080/'
