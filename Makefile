CC = clang

# File names
EXEC = mst_rfx_filter

SOURCES =
OBJECTS = $(SOURCES:.c=.o)

CFLAGS = -O3 -W -Wall -pedantic -std=c99 -I. -Iinclude/ -I/opt/local/include/
LIBS = -lc -lzmq
LDFLAGS = -L/opt/local/lib/ $(LIBS)

.PHONY: all clean

all: $(EXEC)

# To obtain object files
%.o: %.c
	$(CXX) -c $(CXXFLAGS) $< -o $@

# Main target
$(EXEC): $(OBJECTS) $(EXEC).c
	$(CC) $(CFLAGS) $(EXEC).c $(OBJECTS) $(LDFLAGS) -o $(EXEC)

# To remove generated files
clean:
	rm -f $(EXEC) $(OBJECTS)
