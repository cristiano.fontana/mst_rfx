/*
 * (C) Copyright 2016 Cristiano Lino Fontana
 */

// This macro is to use nanosleep even with compilation flag: -std=c99
#define _POSIX_C_SOURCE 199309L
// This macro is to enable snprintf() in macOS
#define _C99_SOURCE

// For nanosleep()
#include <time.h>
// Fot getopt
#include <getopt.h>
// For malloc
#include <stdlib.h>
// For memcpy
#include <string.h>
// For ints with fixed size
#include <stdint.h>
#include <inttypes.h>
// For kernel signals management
#include <signal.h>
// For snprintf()
#include <stdio.h>
// For boolean datatype
#include <stdbool.h>
// For the time functions for ISO time
#include <time.h>

#include <zmq.h>

#include "defaults.h"
#include "events.h"
#include "socket_functions.h"

#define DEFAULT_PUB_ADDRESS "tcp://*:17181"

unsigned int terminate_flag = 0;

// Handle standard signals
// SIGTERM (from kill): terminates kindly forcing the status to the closing branch of the state machine.
// SIGINT (from ctrl-c): same behaviour as SIGTERM
// SIGHUP (from shell processes): same behaviour as SIGTERM

void signal_handler(int signum)
{
    if (signum == SIGINT || signum == SIGTERM || signum == SIGHUP)
    {
        terminate_flag = 1;
    }
}

void print_usage(const char *name) {
    printf("Usage: %s [options] <reference_channels>\n", name);
    printf("\n");
    printf("Datastream filter that selects the events that are in coincidence with a set of channels in a defined time window.\n");
    printf("\n");
    printf("Optional arguments:\n");
    printf("\t-h: Display this message\n");
    printf("\t-v: Set verbose execution\n");
    printf("\t-V: Set verbose execution with more output\n");
    printf("\t-S <address>: SUB socket address, default: %s\n", defaults_abcd_data_address_sub);
    printf("\t-P <address>: PUB socket address, default: %s\n", DEFAULT_PUB_ADDRESS);
    printf("\t-T <period>: Set base period in milliseconds, default: %d\n", defaults_cofi_base_period);
    printf("\t-r <right_coincidence_window>: Right edge of coincidence window in milliseconds, default: %f\n", defaults_cofi_coincidence_window_right);
    printf("\t-n <ns_per_sample>: Nanoseconds per sample, default: %f\n", defaults_cofi_ns_per_sample);

    return;
}

enum {
    SEARCHING_TRIGGER,
    FOUND_TRIGGER,
    OUT_OF_WINDOW
};

bool in(int channel, int *reference_channels, size_t number_of_channels);
void quicksort(struct event_PSD *events, intmax_t low, intmax_t high);
intmax_t partition(struct event_PSD *events, intmax_t low, intmax_t high);
struct event_PSD max_timestamp(const struct event_PSD *events, size_t events_number);

int main(int argc, char *argv[])
{
    // Register the handler for SIGTERM (from kill), SIGINT (from ctrl-c)
    signal(SIGTERM, signal_handler);
    signal(SIGINT, signal_handler);
    signal(SIGHUP, signal_handler);

    unsigned int verbosity = 0;
    unsigned int base_period = defaults_cofi_base_period;
    char *input_address = defaults_abcd_data_address_sub;
    char *output_address = DEFAULT_PUB_ADDRESS;
    float right_coincidence_window_ms = defaults_cofi_coincidence_window_right;
    float ns_per_sample = defaults_cofi_ns_per_sample;

    int c = 0;
    while ((c = getopt(argc, argv, "hS:P:T:r:n:vV")) != -1) {
        switch (c) {
            case 'h':
                print_usage(argv[0]);
                return EXIT_SUCCESS;
            case 'S':
                input_address = optarg;
                break;
            case 'P':
                output_address = optarg;
                break;
            case 'T':
                base_period = atoi(optarg);
                break;
            case 'r':
                right_coincidence_window_ms = atof(optarg);
                break;
            case 'n':
                ns_per_sample = atof(optarg);
                break;
            case 'v':
                verbosity = 1;
                break;
            case 'V':
                verbosity = 2;
                break;
            default:
                printf("Unknown command: %c", c);
                break;
        }
    }

    const int number_of_references = argc - optind;

    if (number_of_references <= 0)
    {
        print_usage(argv[0]);
        return EXIT_SUCCESS;
    }

    int *reference_channels = (int *)malloc(number_of_references * sizeof(int));

    for (int i = 0; i < number_of_references; i++)
    {
        reference_channels[i] = atoi(argv[optind + i]);
    }

    const intmax_t right_coincidence_window = (intmax_t)(right_coincidence_window_ms * 1e6 / ns_per_sample);

    if (verbosity > 0) {
        printf("Input socket address: %s\n", input_address);
        printf("Output socket address: %s\n", output_address);
        printf("Selected channel(s): ");
        for (int i = 0; i < number_of_references; i++)
        {
            printf("\t%d", reference_channels[i]);
        }
        printf("\n");
        printf("Verbosity: %u\n", verbosity);
        printf("Base period: %u\n", base_period);
        printf("Right coincidence window: %f, (clock steps: %" PRIdMAX ")\n", right_coincidence_window_ms, right_coincidence_window);
        printf("Nanoseconds per sample: %f\n", ns_per_sample);
    }

    // Creates a �MQ context
    void *context = zmq_ctx_new();
    if (!context)
    {
        printf("ERROR: ZeroMQ Error on context creation");
        return EXIT_FAILURE;
    }

    void *input_socket = zmq_socket(context, ZMQ_SUB);
    if (!input_socket)
    {
        printf("ERROR: ZeroMQ Error on input socket creation\n");
        return EXIT_FAILURE;
    }

    void *output_socket = zmq_socket(context, ZMQ_PUB);
    if (!output_socket)
    {
        printf("ERROR: ZeroMQ Error on output socket creation\n");
        return EXIT_FAILURE;
    }

    const int is = zmq_connect(input_socket, input_address);
    if (is != 0)
    {
        printf("ERROR: ZeroMQ Error on input socket binding: %s\n", zmq_strerror(errno));
        return EXIT_FAILURE;
    }

    const int os = zmq_bind(output_socket, output_address);
    if (os != 0)
    {
        printf("ERROR: ZeroMQ Error on output socket connection: %s\n", zmq_strerror(errno));
        return EXIT_FAILURE;
    }

    // Subscribe to data topic
    zmq_setsockopt(input_socket, ZMQ_SUBSCRIBE, defaults_abcd_data_events_topic, strlen(defaults_abcd_data_events_topic));

    // Wait a bit to prevent the slow-joiner syndrome
    struct timespec slow_joiner_wait;
    slow_joiner_wait.tv_sec = 0;
    slow_joiner_wait.tv_nsec = defaults_all_slow_joiner_wait * 1000000L;
    nanosleep(&slow_joiner_wait, NULL);
    //usleep(defaults_all_slow_joiner_wait * 1000);

    // Setting up the wait for the main loop
    // If you're asking why, on earth, are we using something so complicated,
    // on the man page of usleep it is said:
    // 4.3BSD, POSIX.1-2001.  POSIX.1-2001  declares  this  function  obsolete;  use  nanosleep(2)
    struct timespec wait;
    wait.tv_sec = base_period / 1000;
    wait.tv_nsec = (base_period % 1000) * 1000000L;

    size_t counter = 0;
    size_t msg_counter = 0;
    size_t msg_ID = 0;

    int search_state = SEARCHING_TRIGGER;
    struct event_PSD *events_buffer = NULL;
    size_t events_buffer_size = 0;
    struct event_PSD trigger_event;

    while (terminate_flag == 0)
    {
        char *topic;
        char *input_buffer;
        size_t size;

        const int result = receive_byte_message(input_socket, &topic, (void **)(&input_buffer), &size, true, verbosity);

        if (result == EXIT_FAILURE)
        {
            printf("[%zu] ERROR: Some error occurred!!!\n", counter);
        }
        else if (size == 0 && result == EXIT_SUCCESS)
        {
            if (verbosity > 2)
            {
                printf("[%zu] No message available\n", counter);
            }
        }
        else if (size > 0 && result == EXIT_SUCCESS)
        {
            if (verbosity > 0)
            {
                printf("[%zu] Message received!!! (topic: %s)\n", counter, topic);
            }

            // Check if the topic is the right one
            if (strstr(topic, "data_abcd_events_v0") == topic)
            {
                const clock_t event_start = clock();

                const size_t events_number = size / sizeof(struct event_PSD);

                struct event_PSD *events = (void *)input_buffer;

                if ((size % sizeof(struct event_PSD)) != 0)
                {
                    printf("[%zu] ERROR: The buffer size is not a multiple of %zu\n", counter, sizeof(struct event_PSD));
                }
                else
                {
                    if (search_state == SEARCHING_TRIGGER)
                    {
                        // We will see if in this event we have a trigger signal
                        for (size_t i = 0; i < events_number; i++)
                        {
                        
                            const struct event_PSD this_event = events[i];

                            if (in(this_event.channel, reference_channels, number_of_references))
                            {
                                if (verbosity > 0)
                                {
                                    printf("[%zu] Found trigger!!! Timestamp: %" PRIu64", %f ms\n", counter, this_event.timestamp, this_event.timestamp * ns_per_sample * 1e-6);
                                }

                                trigger_event = this_event;
                                search_state = FOUND_TRIGGER;

                                break;
                            }
                        }
                    }

                    // No else clause so it can end the accumulation
                    // even if this message covers the whole window
                    if (search_state == FOUND_TRIGGER)
                    {
                        // We will extend our events buffer to add the new message, then we can sort it
                        if (verbosity > 0)
                        {
                            printf("[%zu] Extending buffer from %zu to %zu (by %zu)\n", counter, events_buffer_size, events_buffer_size + events_number, events_number);
                        }

                        struct event_PSD *new_buffer = (struct event_PSD *)malloc(sizeof(struct event_PSD) * (events_buffer_size + events_number));

                        if (!new_buffer)
                        {
                            printf("[%zu] ERROR: Unable to allocate new events buffer\n", counter);
                        }
                        else
                        {
                            memcpy(new_buffer, events_buffer, sizeof(struct event_PSD) * events_buffer_size);
                            memcpy(new_buffer + events_buffer_size, events, sizeof(struct event_PSD) * events_number);
                            events_buffer_size += events_number;

                            if (events_buffer)
                            {
                                free(events_buffer);
                            }

                            events_buffer = new_buffer;

                            search_state = FOUND_TRIGGER;

                            const struct event_PSD max_event = max_timestamp(events_buffer, events_buffer_size);

                            const intmax_t delta_time = (intmax_t)max_event.timestamp - (intmax_t)trigger_event.timestamp;
                            if (delta_time > right_coincidence_window)
                            {
                                if (verbosity > 0)
                                {
                                    printf("[%zu] We are out of the coincidence window! (delta_time: %" PRIdMAX ", %f ms)\n", counter, delta_time, delta_time * ns_per_sample * 1e-6);
                                }

                                search_state = OUT_OF_WINDOW;
                            }
                            else if (verbosity > 0)
                            {
                                printf("[%zu] We are still in the coincidence window (delta_time: %" PRIdMAX ", %f ms)\n", counter, delta_time, delta_time * ns_per_sample * 1e-6);
                            }
                        }
                    }

                    // Again no else
                    if (search_state == OUT_OF_WINDOW)
                    {
                        if (verbosity > 0)
                        {
                            printf("[%zu] Out of window analysis...\n", counter);
                        }

                        if (!events_buffer)
                        {
                            printf("[%zu] ERROR: Empty events buffer out of window\n", counter);
                        }
                        else
                        {
                            // We will now recalculate all the timestamps according to the triggering event
                            //for (size_t i = 0; i < events_buffer_size; ++i)
                            //{
                            //    events_buffer[i].timestamp = events_buffer.timestamp[i] - trigger_event.timestamp;
                            //}

                            if (verbosity > 0)
                            {
                                printf("[%zu] Sorting buffer...\n", counter);
                            }

                            quicksort(events_buffer, 0, events_buffer_size - 1);

                            if (verbosity > 0)
                            {
                                printf("[%zu] Sorted!\n", counter);
                            }

                            if (verbosity > 0)
                            {
                                const struct event_PSD last_event = events_buffer[events_buffer_size - 1];
                                const float shot_length_ms = (last_event.timestamp - trigger_event.timestamp) * ns_per_sample * 1e-6;

                                printf("[%zu] Shot length: %f ms\n", counter, shot_length_ms);
                            }

                            const size_t output_size = events_buffer_size * sizeof(struct event_PSD);

                            // Compute the new topic
                            char new_topic[defaults_all_topic_buffer_size];
                            // I am not sure if snprintf is standard or not, apprently it is in the C99 standard
                            snprintf(new_topic, defaults_all_topic_buffer_size, "data_abcd_events_v0_s%zu", output_size);

                            send_byte_message(output_socket, new_topic, (void *)events_buffer, output_size, verbosity);
                            msg_ID += 1;

                            if (verbosity > 0)
                            {
                                printf("Sending message with topic: %s\n", new_topic);
                            }

                            search_state = SEARCHING_TRIGGER;
                            free(events_buffer);

                            events_buffer = NULL;
                            events_buffer_size = 0;
                        }
                    }
                }

                const clock_t event_stop = clock();

                if (verbosity > 0)
                {
                    const float elaboration_time = (float)(event_stop - event_start) / CLOCKS_PER_SEC * 1000;
                    const float elaboration_speed = size / elaboration_time * 1000.0 / 1024.0 / 1024.0;

                    printf("size: %zu; events_number: %zu; events_buffer_size: %zu; elaboration_time: %f ms; elaboration_speed: %f MBi/s\n", size, events_number, events_buffer_size, elaboration_time, elaboration_speed);
                }
            }

            msg_counter += 1;

            // Remember to free buffers
            free(topic);
            free(input_buffer);
        }
        else
        {
            printf("[%zu] ERROR: What?!?!?!\n", counter);
        }

        counter += 1;

        // Putting a delay in order not to fill-up the queues
        nanosleep(&wait, NULL);
        //usleep(base_period * 1000);

        if (verbosity > 2)
        {
            printf("counter: %zu; msg_counter: %zu, msg_ID: %zu\n", counter, msg_counter, msg_ID);
        }
    }

    free(reference_channels);

    // Wait a bit to allow the sockets to deliver
    nanosleep(&slow_joiner_wait, NULL);
    //usleep(defaults_all_slow_joiner_wait * 1000);

    const int ic = zmq_close(input_socket);
    if (ic != 0)
    {
        printf("ERROR: ZeroMQ Error on input socket close: %s\n", zmq_strerror(errno));
        return EXIT_FAILURE;
    }
    
    const int oc = zmq_close(output_socket);
    if (oc != 0)
    {
        printf("ERROR: ZeroMQ Error on output socket close: %s\n", zmq_strerror(errno));
        return EXIT_FAILURE;
    }
    
    const int cc = zmq_ctx_destroy(context);
    if (cc != 0)
    {
        printf("ERROR: ZeroMQ Error on context destroy: %s\n", zmq_strerror(errno));
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

bool in(int channel, int *reference_channels, size_t number_of_channels)
{
    for(size_t i = 0; i < number_of_channels; i++)
    {
        if (channel == reference_channels[i])
        {
            return true;
        }
    }

    return false;
}

void quicksort(struct event_PSD *events, intmax_t low, intmax_t high)
{
    if (low < high)
    {
        const intmax_t pivot_index = partition(events, low, high);

        //printf("Quicksort: low: %lu, high: %lu, pivot: %lu\n", low, high, pivot_index);

        quicksort(events, low, pivot_index - 1);
        quicksort(events, pivot_index + 1, high);
    }
}

intmax_t partition(struct event_PSD *events, intmax_t low, intmax_t high)
{
    const struct event_PSD pivot = events[high];

    intmax_t i = low - 1;

    for (intmax_t j = low; j <= high - 1; j++)
    {
        if (events[j].timestamp <= pivot.timestamp)
        {
            i += 1;

            const struct event_PSD temp = events[j];
            events[j] = events[i];
            events[i] = temp;
        }
    }

    const struct event_PSD temp = events[high];
    events[high] = events[i + 1];
    events[i + 1] = temp;
    
    return i + 1;
}

struct event_PSD max_timestamp(const struct event_PSD *events, size_t events_number)
{
    if (events_number > 0)
    {
        struct event_PSD selected_event = events[0];

        for (size_t i = 1; i < events_number; ++i)
        {
            if (events[i].timestamp > selected_event.timestamp)
            {
                selected_event = events[i];
            }
        }

        return selected_event;
    }
    else
    {
        const struct event_PSD empty = {0, 0, 0, 0, 0, 0};
        return empty;
    }
}
